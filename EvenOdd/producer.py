import sys, pika, os


connection = pika.BlockingConnection(
pika.ConnectionParameters(host='localhost')) 
channel = connection.channel() # start a channel
channel.queue_declare(queue='numbers') # Declare a queue
# send a message

message= sys.argv[1]
channel.basic_publish(exchange='', routing_key='numbers', body=message)
print ("[x] Message sent to consumer")
connection.close()
