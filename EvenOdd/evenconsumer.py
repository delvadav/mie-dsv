import pika, os

connection = pika.BlockingConnection(
pika.ConnectionParameters(host='localhost')) 
channel = connection.channel() # start a channel
channel.queue_declare(queue='numbers') # Declare a queue
# send a message


# create a function which is called on incoming messages
def callback(ch, method, properties, body):
  if(int(body)%2==0):
  	print(int(body))

# set up subscription on the queue
channel.basic_consume('numbers',
  callback,
  auto_ack=True)

# start consuming (blocks)
channel.start_consuming()
connection.close()
